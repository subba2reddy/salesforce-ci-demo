@isTest
private class StringUtilTest {
	
	@isTest 
	static void testPass() {
		System.assertEquals( 1, StringUtil.countSpaces( 'One Space' ), 'Should be one space!' );	
	}
	
	@isTest 
	static void testFail() {
		System.assertEquals( 3, StringUtil.countSpaces( 'This has three spaces' ), 'Should have three spaces!' );	
		
	}
	
	@isTest 
	static void testPass2() {
		System.assertEquals( 2, StringUtil.countSpaces( 'Two Spaces Here' ), 'Should be two spaces!' );	
		
	}
	
}